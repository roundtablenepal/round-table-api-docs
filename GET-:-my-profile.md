URL : /api/member/get_profile  
    
    
**Note: User has facility to update his profile from Website also. So we cant save profile in local db and show that every time user opens the app. This can show past/out-dated data. To prevent this, each time user goes to "view my profile" section of app, fetch his latest profile from this API and show in app to keep sync.**    
     
Header Param    
Api-Key = 'key'   

POST Body (valid member Id)     
member_id = '317'   
    
Response   
response data is same as response sent by server on successful login. except there is no client Id.. We are no longer using Client_id for authentication..    
**DO Not** Save Member Details in local db. access this API to fetch Profile when user opens view my profile section on app.    
            
Success--    
{   
success: "true"    
error: null    
data: {     
member_info: {        
details: {member_id: 1,table_id: 2,fname: "asd",...}      
}      
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "member profile not found"    
}    
data: null    
}    