URL : /api/convener/create   
    
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     
     
member_id = 11    
name = Demo    
table_code = KRT-1    
designation = Vice President    
email = demo@demo.com    
mobile = 78945678    
details = -    
    
Response        
Success--    
{   
success: "true"    
error: null    
data: {     
   msg: "Convener Created Successfully"        
}     
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "failed due to invalid data"    
}    
data: null    
}    