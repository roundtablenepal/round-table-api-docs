URL : api/member/edit_profile   

**Update: please accommodate two new fields "address" & "company" in profile form**     
      
Now on successful profile edit, this API will return you updated info of member along with path for new image thumb/big**    
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     

**Following fields should be sent for profile update**    
 SAMPLE REQUEST( * fields compulsory)    
 
*(do not accept from user)  member_id = 3   
*(do not accept from user)  table_id = 5   
*email = new.email@yahoo.com   
*fname = Demo123    
lname = demo12   
*gender = male|female      
*mobile = 213456   
*blood_group = AB- (Show select list of 8 possible blood group)    
*spouse_name = Spouse    
*dob = 1989-05-15(calender)    
*spouse_dob = 1989-05-15(calender)   
*anniversary_date = 2015-05-15(calender)   
res_phone = 021-00-123456    
office_phone = 231-00-4567891    
designation = Designation    
res_city = Pune    
office_city = Pune    
state = maharashtra    
address = 32 abc, pqr     
company = abc company     
          
**Optional** :: Image profile File (Multipart form-data)     
field name for image file = **"profile_image"**    
allowed types :: gif|jpg|png       
    
Response        
Success--    
{   
success: "true"    
error: null    
data: {     
   updated_info: {member data obj.}     
}     
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "failed due to incomplete data"    
}    
data: null    
}    