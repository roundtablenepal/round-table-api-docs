API Request     
1.Every API (Including First login request) will have **"Api-Key"** set in header with value provided to you in mail  
2.For API of Login, Request OTP and request access will need only Api-Key in header and **NO** "client_id".   
3.Login API will return **"Client-Id"** on success, this will be required for accessing other APIs (**together with Api-Key**) once user Logged in.      
    
API Response Format (JSON Only)   

----
On Success:    
{    
"success":"true",     
"error":null,    
"data":{data}       
}    
     
----
On Error:    
{    
"success":"false",     
"error":   
{   
"code":err_code,   
"msg":"error message description"   
},    
"data":null   
}