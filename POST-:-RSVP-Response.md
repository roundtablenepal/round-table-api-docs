URL : /api/event/rsvp
    
**Note : I have also changed the request & response of get events & meetings. Server will now send rsvp response status of each member for each event .. Plz check the documentation of Get Event List & Get Meeting List**    
     
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     
     
member_id = member_id (user)     
event_id = event_id    
response = "yes/no/may be" (allowed only these three responses.. case insensitive)         
    
Response        
Success--    
{   
success: "true"    
error: null    
data: {     
   msg: "RSVP updated successfully"     
}     
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "failed due to invalid data"    
}    
data: null    
}    