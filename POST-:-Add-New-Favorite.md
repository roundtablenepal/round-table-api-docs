URL : /api/favorites/create   
    
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     
     
brand_name = 'new brand'    
website_url = 'www.demo.com'    

Multipart form data    
favorites_image =[Image]       
    
Response        
Success--    
{   
success: "true"    
error: null    
data: {     
   msg: "Favorite Created Successfully"        
}     
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "failed due to invalid data"    
}    
data: null    
}    