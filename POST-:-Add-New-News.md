URL : /api/news/create   
    
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     
     
news_headline = "Demo news Headline" (:text)    
news_description = "demo description here" (:text)    
member_id = 12 (valid member_id of app user who is creating a news)     
concern_tables = 1,12,20  (comma separated list of **valid** table_ids to whom this news is concered..)   
    
Image File (Multipart form-data)     
field name for image file = **"news_image"**    
allowed types :: gif|jpg|png       
    
Response        
Success--    
{   
success: "true"    
error: null    
data: {     
   msg: "News Created Successfully"     
}     
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "failed due to invalid data"    
}    
data: null    
}    