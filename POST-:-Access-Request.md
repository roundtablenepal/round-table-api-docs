URL /api/access_request   
    
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     
email = 'email_id'   
name= 'requester name'   
table_name = 'table name'   
    
    
Response   
   
Success--    
{   
success: "true"    
error: null    
data: {    
msg: "Request Submitted Successfully."    
}    
}    
     

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "User already Exists, Request your OTP to login"       
}    
data: null    
}    