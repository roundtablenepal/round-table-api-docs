URL : /api/meeting/get_all   
GET Without Params   
     
     
**Update (Aug 13,2015): This API will now send 4 news keys in response. We have to use these keys for following purpose**     
   
Events/Meetings should be listed with the following 
- Host Table Name ("host" key value)
- Meeting Name
- Venue
- Date
- Time
- Spouse : Invited/Not Invited
- Children : Invited/Not Invited
- put one button saying "View Location on map", on click of which App will open map view and display location pointed by Lat-Lng, provided in server response.      
- RSVP      
      
**New keys in response**   
1. "host": String value containing host data, Just pick the value and display in Event details Screen    
2. "show_rsvp" : true/false (if member is eligible to view responses,this field will be true.) Depending on true/false, app will enable/disable feature of view RSVP on particular event/meeting    
3. "lat" : location data    
4. "lng" : location data
      
        
Last- Update: plz send user's 'Member-Id' in header as mentioned below, so that server will send & track member's RSVP responses for all event/meeting       
Also use new field in response i.e rsvp to show user's response to the event/meeting as following    
1. null - User has not yet responded to event (all three options are open)    
2. yes - User has already replied as 'yes', he can update his response to 'no' or 'may be'.    
3. no - -------||--------(same as above)     
4. may be - -----------------||-------------- (same as above)        
     
Header Param    
Api-Key = 'key'   
Member-Id = member_id     
     
GET      

Response   
   
Success--    
{   
success: "true"    
error: null    
data: {    
meeting_list: [{meeting_obj1},{meeting_obj1},{meeting_obj1}]    
}    
}    
     

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "Access Denied/No Data Available"    
}    
data: null    
}    