URL : /api/event/get_rsvp     
the same API will be used to fetch meeting RSVP also     
**this API is to fetch data of RSVP responses to particular event/meeting that member created from App. Response for particular event/meeting will be visible to only that member who created it**
    
**Note : I have also changed the request & response of get events & meetings. Server will now send "show_rsvp" key,Please check updated document for "event/get_all" and "meeting/get_all"**    
     
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     
     
member_id = member_id (user)     
event_id = event_id (for event/meeting)    
    
Response        
Each object inside "rsvp_response" will be a member info containing name, table code, rsvp response, and response date.     
Currently display a "list-view" of responses containing name, response and response date.     
Success--    
{   
success: "true"    
error: null    
data: {     
   rsvp_response: [{response_obj1},{response_obj2},{response_obj3}]     
}     
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "No response received"/"failed due to invalid data"    
}    
data: null    
}    