URL : /api/member/search      
    
Header Param    
Api-Key = 'key'   

POST (To handle special characters) (validation should be done on app side)     
searchBy = '**search category**'     
searchKey= 'key to search'   
    
(Currently Supporting **search category** for search) **name/email/city/mobile/blood_group/date**   
    
name -- will search match for fname, lname,mname    
date -- will search dob & anniversary date: format required for search (YYYY-MM-DD eg. 2015-06-18)           

Response   
   
Success--    
{   
success: "true"    
error: null    
data: {    
members_list: [{member_obj1},{member_obj1},{member_obj1}]    
}    
}    
     

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "Access Denied/No Data Available"    
}    
data: null    
}    