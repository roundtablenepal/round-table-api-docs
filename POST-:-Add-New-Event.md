URL : /api/event/create   
    
Header Param    
Api-Key = 'key'   
    
**Update (Aug 13,2015)**   
there will be a additional provision for user, while creating a event User can select Venue location on map.    
for this, we need to provide a button, on click of which user will go to a map screen (within the app only) and select a location by searching or by navigating (**same as location feature on whatsapp**). And we need to capture  latitude and Longitude of location and send in POST data of this API.   
      
POST Body (validation should be done on app side)     
     
event_type = "event" (for event)     
event_name = "RTN event demo" (Name:text)    
event_venue = "Kathmandu" (venue:text)    
event_date = 2015-06-06 (YYYY-MM-DD)    
event_time = 12.03.00 (HH.MM.SS)    
invites = 2,5,3 (string of concern table_ids sapearted by ','(comma))    
is_spouse = 1/0 (1='true', 0='false')   
is_children = 1/0   
member_id = 5 (Member_id of app user )   
**venue_location = address details described by G-Map for selected lat-lng**     
**lat = latitude from map location**    
**lng = longitude from map location**    

     
    
        
Image File (Multipart form-data)(optional)     
field name for image file = **"event_image"**    
allowed types :: gif|jpg|png       
    
Response        
Success--    
{   
success: "true"    
error: null    
data: {     
   msg: "Event/meeting Created Successfully"     
}     
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "failed due to invalid data"    
}    
data: null    
}    