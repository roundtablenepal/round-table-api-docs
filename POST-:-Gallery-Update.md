URL /api/gallery/post_update     
     
**We need to call this API from APP side, whenever user uploads any image to Cloudinary Server update our API server with required data so that uploads can be tracked**        
    
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     
member_id = 12    
image_name = adgddsf5sd6.jpg (image name on cloudinary server)    
image_desc = test upload 2 (get this from user)        
    
    
Response   
   
Success--    
{   
success: "true"    
error: null    
data: {    
msg: "Gallery Update Successful."    
}    
}    
     

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "error in update set/ access denied"       
}    
data: null    
}    