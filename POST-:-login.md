URL : /api/login   
    
Header Param    
Api-Key = 'key'   

POST Body (validation should be done on app side)     
email = 'email_id'   
otp = 'otp'   
os = 'gcm/apn'   
token = 'gcm_id/apn_id'    
    
    
Response   
Client_id will be required as header param for every subsequent API call.    
Save Member Details in local db. And access Update Profile API call when user edits his profile.        
Success--    
{   
success: "true"    
error: null    
data: {     
member_info: {     
client_id: "a1b2c3",     
details: {member_id: 1,table_id: 2,fname: "asd",...}      
}      
}     
      

Error--    
{    
success: "false"    
error: {   
code: 402    
msg: "OTP invalid"    
}    
data: null    
}    